
# league app
iniciar una nueva aplicacion league

models.py
```
	class League(models.Model):
		name = models.CharField(max_length=24)
		level = models.CharField(max_length=24)
		number_pokemons = models.IntegerField()
		number_medals = models.IntegerField()
		number_trainers = models.IntegerField()

		def __str__(self):
			return self.name
```

añadir aplicación
y correr migraciones

views.py
```
	# añadir modelos
	class LeagueList(generic.ListView):
		template_name = "league/list.html"
		model = League
```

league/list.html
```
 <!-- imprimir lista de ligas pokemon -->
```

routes.html
```
	path('list/',views.LeagueList.as_view(),name='league_list')
```
añadir ruta para la aplicacion en el routeador principal

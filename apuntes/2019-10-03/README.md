# logout y sign up

base.html
```
	<!-- agregar opcion al nav bar para la url log out  -->
	
```

urls.py
```
	from django.contrib.auth.views import logout_then_login
	# agregar url para log out
	path('cerrar/', logout_then_login, name='logout')
	path('signup/', views.Signup.as_views(), name='signup')

```

setting.py
```
	LOGIN_URL = reverse_lazy('login')
	LOGIN_REDIRECT_URL = reverse_lazy('login') 
	LOGOUT_URL = reverse_lazy('logout')
```

views.py
```
	class Signup(generic.FormView):
		template_name = 'home/signup.html'
		form_class = Users_Form
		success_url = reverse_lazy('login')

		def form_valid():
			

```

signup.html
```
	{% extends 'base.html' %}
		{% block title %}Signup{% endblock %}
		{% block content %}
			<form method = "POST">
				{% csrf_token %}
				<button type="submit" class="btn btn-primary">Sign up!</button>
				<button type="reset" class="btn btn-danger">Cancelar</button>
			</form>
		{% endblock %}
```

forms.py
```	
	from django.contrib.auth.forms import UserCreationForm
	class Users_Form(UserCreationForm):
		phone = forms.IntegerField()
		email = forms.CharField()
		address = forms.CharField()
```

# Busqueda
En esta clase implementamos las busquedas en la aplicación de pokedex.

base.html
```
	<script type="text/javascript">
		$(document).ready(function(){
			var searchInput = $("#nav-bar-form input[type=search]");
			var searchQuery;
			
			searchInput.keyup(function(event){
				searchQuery = $(this).val();
			});

			function doneSearchTyping(){
				if(searchQuery){
					var url = "/pokedex/search?q=" + searchQuery;
					document.location.href = url;
				}
			}
		});
	</script>
```

views.py
```
	from django.db.models import Q
	# class List(generic.ListView)
	def get_queryset(self, *args, **kwargs):
		qs = Pokemon.object.all()
		print(self.request)
		query = self.request.GET.get("q",None)
		print(query)
		if query is not None:
			qs = qs.filter(Q(trainer__username__icontains=query) | Q(pokemon_name__icontains=query))
		return qs
```

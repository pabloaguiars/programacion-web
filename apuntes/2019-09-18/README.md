# API(ENDPOINT) 
END POINT DE LECTURA. Construcción manual del API.

Instalar DjangoRESTFramework! pokedex

views.py
```
	from django.core import serializers
	from django.http import JsonResponse, HttpResponse

	#class retrive
	def wsList(request):
		# json
		# data = serializers.serialize('json', Pokemon.object.all())		
		# return HttpResponse(request,content_type='application/json')
		
		# xml
		data = serializers.serialize('xml', Pokemon.object.all())
                return HttpResponse(request,content_type='application/xml')
			
```

urls.py
```
	path('wslist', views.wsList, name='wslist')
```

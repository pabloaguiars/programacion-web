# Managers

´´´python

class Pokemon_QuerySet(models.QuerySet):
  def pokemon_fire(self):
    return self.filter(pokemon_type='Fire')

  def pokemon_electric(self):
    return self.filter(pokemon_type='Electric')

class Pokemon(models.Model):
  objects = Pokemon_QuerySet.as_manager() # add this new field
´´´

se pude mandar a llamar un método del query set como entrada a otro metodo del query set.

´´´pyhton
  Pokemon.objects.all().order('pokemon_name').pokemon_fire().pokemon_level(1).count()
´´´

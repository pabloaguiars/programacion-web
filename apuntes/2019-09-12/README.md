
# Create con vistas genericas
create2.html
```
	#misma plantilla que create 1
	
```

urls.py
```
	#add urls for create2.html
```

views.py
```
	from django.urls import reverse_lazy

	class Create(generic.CreateView):
		#agregar esto a la la clase que se creo en la clase pasada
		template_name = 'pokedex/create2.html'
		model = Pokemon
		fields = [
			'pokemon_name',
			'pokemon_type',
			...
		]
		success_url = reverse_lazy('list')
```

nota: está prohibido crear una clase mixin sin los fields

# Delete pokemon con vistas genericas
views.py
```
	class Delete(generic.DeleteView):
		template_name = 'pokedex/delete2'
		model = Pokemon
		
		success_url = reverse_lazy('list')
```

delete2.html
```
	#mismo código que delete1.html
```

urls.py
```
	#add path to urls, delete2 <int:pk>
```

# Edit nav bar para que funcione con las urls de urls.py de la app

# Update con generic views

views.py
```
	class Update(generic.UpdateView):
		template_name = 'pokedex/create2.html'
		model = Pokemon
		fields = [
			''
		]
		success_urls = reverse_lazy('list')
```

update2.html
```
	<!-- igual que update2  -->
```

urls.py
```
	#add path para update2.html, no olvidar el <int:pk>
```

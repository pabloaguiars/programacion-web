# djangorestframework
instalar djangorestframework como se disce desde el sitio web oficial

en terminal
```
	pip install djangorestframework markdown django-filter
``` 

serializers.py
```
	 # rest framework
        from rest_framwork import serializers
        from django.contrib.auth.models	import User 
	from pokedex.models import Pokemon

        class UserSerializer(serializers.ModelSerializer):
                class Meta:
                        model =	User
                        fields = [ 
                                'id',
                                'username',
                                'lastname,
                                'email',
                        ]
	
	class PokemonSerializer(serializers.ModelSerializer):
                class Meta:
                        model = User
                        fields = [
                                'id',
                                'pokemon_name',
                                'pokemon_type,
                                'trainer',
                        ] 

```

views.py
```
	from rest_framework import viewset
	from home.serializers import UserSerializer
	from home.serializers import PokemonSerializer
	from django.contrib.auth.models import User
	from pokedex.models import Pokemon	
	from rest_framework import generics
	
	class UserListAPIView(viewset.ListAPIView):
		queryset = User.object.all().order_by('-date_joined')
		serializer_class = UserSerializer
		# profesor uso get_queryset

        class PokemonListAPIView(viewset.ListAPIView):
                queryset = Pokemon.object.all().order_by('-date_joined')
                serializer_class = PokemonSerializer 

```

project/urls.py
```
	path('api-auth/',include('rest_framewokr.urls')
```

app/urls.py
```
	path('endpoint-users/', views.UserListAPIView, name='UserSerializer'),
	path('endpoint-pokemons/', views.PokemonListAPIView, name='PokemonSerializer'),

```

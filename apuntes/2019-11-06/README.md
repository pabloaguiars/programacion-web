# testing

test.py
```python
from django.test import TestCase

class TestPokemonCreated(TestCase):
  def setup(self):
    self.username = 'some_user'
    new_pokemon = Pokemon.objects.create(
      train=self.username,
      pokemon_name='some_pokemon',
      pokemon_type='plant'
      pokemon_level=1,
      pokemon_exp=1000,
      status=True,
      slug=self.username
    )

  def test_created(self):
    pokemon_created = Pokemon.objects.get(pokemon_name='some_pokemon')
    self.assertTrue(pokemon_created.exist())
```

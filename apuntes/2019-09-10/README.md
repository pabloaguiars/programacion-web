# CRUD dificil; list
Creamos el READ del CRUD con vistas genericas

views.py
```
	class List(generic.ListView):
		template_name = 'pokedex/list2.html'
		model = Pokemon
	
	class Detail(generic.DetailView):
                template_name = 'pokedex/detail2.html'
                model = Pokemon

```

list2.html
```
	<!-- mismo código que list.html -->
		<!-- for pokemon in object_list --> 
```

detail2.html
```
	<!-- mismo código que list.html -->
                <!-- for pokemon in object_list -->
```

urls.py
```
	path('list2/', views.List.as_view,name='list2'),
	path('detail2/<int:pk>/', views.Detail.as_view,name='detail2'),

```

# CRUD dificil; manipulacion del contexto y el queryset

views.py
```
	class List(generic.ListView):
                template_name = 'pokedex/list2.html'
                model = Pokemon
		
		def get_context_data(self, *args, **kwargs):
			context = super(List, self).get_context_data(*args, **kwargs) # context = object_list | context = get_context_data()
			context['message'] = 'XD'
			return context
		
		def get_query_set(self, *args, **kwargs):
			qs = Pokemon.object.all()
                        # context = super(List, self).get_context_data(*args, **kwargs) # context = object_list | context = get_context_data()
                        # context['message'] = 'XD'     
                        return qs

```

un diccionario es un keyword arguments

en esta prÁctica contruimos el formulario create pokedex automaticamente desde django

urls.py
	path("create/",views.create,name="create")

views.py
	def create(request)
		form = PokemonForm(request.POST or None)
		if request.user.is_authenticated:
			message = "User Is Logged"
			if form.is_valid():
				instance = form.save(commit=False)
				instance.trainer = request.user
				instance.save()
		else:
			message = "User must Be Logged"
			
		context = {
			"form": form
			"mesage":message
		}
		
		return render(request,"pokedex/create.html",context)
forms.py
	
	from django import forms
	from .models import Pokemon
	
	class PokemonForm(forms.ModelForm)
		trainer = model.ForeingKey(settings.AUTH_USER_MODEL, on_delete=model.CASCADE, default=1)
		pokemon_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"","class":""}))
		... pokemon_level = forms.IntegerField(widget=forms.""(attrs={"placeholder":"","class":""}))
		model=Pokemon
		fields = [
			"pokemon_name",
			"pokemon_type",
			...
		]
create.html
	{% csrf_token  %}
	<form>
		{{ form.as_p }}
		<button> </button>
	</form>

	{{message}}


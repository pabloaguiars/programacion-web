# archivo tomado como referencias, para esta clase, evaluar como se modificó desde la clase pasada
# para funcionar con el post del mismo endpoint del api
# create api

views.py
´´´
	class PokemonCreateAPIView(generics.CreateAPIView):
		serializer_class = PokemonSerializer
		
		def perform_create(self,serializer):
			serializer.save(trainer=self.request.user)
´´´

urls.py
´´´
	path('endpoint-pokemon/create',views.PokemonCreateAPIView.as_view(),name'PokemonCreateSerializer')
´´´

serializer.py
´´´
	# add campos to PokemonSerializer
´´´

# serializar formulario (testing)
create.html
´´´
	<!-- Agregar despues de incluir el formulario form_create.html ->
	<script type="text/javascript">
		$("#pokemon_form").submit(function(event){
			event.preventDefault();
			var thiss_ = $(this);
			var formData = this_.serialize();
			console.log(formData);

		$.ajax({
			url:"/*<url del endpoint>*/",
			method: "POST",
			data: formData,
			success: function(data){
				console.log(data);
			},
			error:  function(data){
				console.log(error);
				console.log(data);
			}
		})
	</script>

	<!-- agregar id al formulario -->
´´´

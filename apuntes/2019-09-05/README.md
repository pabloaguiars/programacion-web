# pokedex update
**views.py**
 
	```
	#from ... import ..., redirect
	def update(request,id):
	pokemon = Pokemon.objects.get(id=id)
	if request.method == 'GET':
		form = PokemonForm(instance=pokemon)
	else:
		form = PokemonForm(request.POST, instance=pokemon)
		if form.is_valid():
			form.save()
		return redirect('list')
	context = {
		'form':form
	}
	return render(request,'pokedex/update.html',context)	
	´´´

**update.html**
	```
	<!-- es el mismo formulario que create.html -->
	´´´

**urls.py**
	```
	path('update/',views.update,name='update')
	´´´

# pokedex delete
**views.py**
	```
	def delete(request,id):
		context={}
		return render(request,'pokedex/update.html',context)
	´´´

**urls.py**
	```
        path('delete/<int:id>',views.delete,name='delete')
        ´´´

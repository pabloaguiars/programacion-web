# app tournament
crear una nueva aplicación de torneos
```
python manage.py startapp torunament
```
añadir aplicación al settings

añadir ruta a la aplicación

urls.py
```
from django.urls import path
from tournament import views

urlpatterns = {
  path ('list_tournament/',views.TorunamentList.as_view(),name='list_tournament'),
}
```

forms.py
```
```

models.py
```
from django.conf import settings
from pokedex.models import Pokemon
from league.models import League

class Tournament(models.Model):
  name = models.CharField(max_length=24)
  trainer = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
  pokemons = models.ManyToManyField(Pokemon)
  league = models.ForeignKey(League,on_delete=models.CASCADE)

  def __str__(self):
    return self.name
```
admin.py
```
# añadir model Tournament al admin site
@admin.register(Tournament)
class AdminPokemon(admin.):
  list_display=[
    'name',
    'trainer',
    'league'
    #'pokemons',
  ]
```
views.py
```
from django.views import generic
from .models import Tournament
class TournamentList(generic.ListView):
  template_name='tournament/list_tournament.html'
  model = Tournament
```

list_tournament.html
```
<!--
agregar listado de torneos


no olvidar usar un iterador para recorre los pokemones xd
-->
```

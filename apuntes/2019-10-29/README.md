# validadores externos

models.py
```python
def validator_pokemon_types(value)
  pokemon_type = value

  pokemons_types = (
    'FIRE',
    'WATER',
    'PLANT',
    'ELECTRIC',
  )

  if pokemon_type.upper() not in types:
    raise ValidationError('Pokemon type does not exists')

  return pokemon_type
```

models.py class Pokemon
```python
from django.core.exceptions import ValidationError

class Pokemon(models.Model):
  pokemon_type = models.CharField(maxlength=25, validators=[validator_pokemon_types])

```

también se pueden mandar a llamar desde un archivo externo

validators.py
```python
from .validators import validator_pokemon_types
```

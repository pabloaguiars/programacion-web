from django.db import models

# Create your models here.
class Pokemon(models.Model):
    pokemon_name = models.CharField(max_length=24)
    pokemon_type = models.CharField(max_length=24)
    pokemon_level = models.IntegerField()
    pokemon_experence = models.IntegerField()
    status = models.BooleanField(default=True)
    timestamp = models.DateField(auto_now_add=True)
    slug = models.SlugField()

    def __str__(self):
        return self.pokemon_name

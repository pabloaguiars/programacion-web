from django.contrib import admin
from pokedex.models import Pokemon

# Register your models here.
@admin.register(Pokemon)
class AdminPokemon(admin.ModelAdmin):
        list_display = [
            'pokemon_name',
            'pokemon_type',
            'pokemon_level'
        ]

from django.shortcuts import render, redirect
from .models import Checkout
from .forms import CheckoutForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = Checkout.objects.all()
    context = {
        'checkouts':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = Checkout.objects.get(id=id)
    context = {
        'checkout':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = CheckoutForm(request.POST or None)
    if request.user.is_authenticated:
        message = 'User is logged'
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Checkout saved'
            else:
                message = 'Invalid form'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    checkout = Checkout.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = checkout
        message = 'User is logged'
        if request.method == 'POST':
            form = CheckoutForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Checkout updated'
            else:
                message = 'Invalid form'
        else:
            form = CheckoutForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    checkout = Checkout.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = checkout
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Checkout deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

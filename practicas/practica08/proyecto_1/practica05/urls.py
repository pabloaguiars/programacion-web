from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='checkouts_index'),
    path('list/',views.list,name='checkouts_list'),
    path('details/<int:id>',views.details,name='checkouts_details'),
    path('create/',views.create,name='checkouts_create'),
    path('update/<int:id>',views.update,name='checkouts_update'),
    path('delete/<int:id>',views.delete,name='checkouts_delete'),
]

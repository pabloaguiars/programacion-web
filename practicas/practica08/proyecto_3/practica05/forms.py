from django import forms
from .models import QuejaSugerencias

class QuejaSugerenciasForm(forms.ModelForm):
    class Meta:
        model = QuejaSugerencias
        fields = [
            'subject',
            'name',
            'email',
            'company',
            'phone',
            'message',
        ]

from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='suggestions_index'),
    path('list/',views.list,name='suggestions_list'),
    path('details/<int:id>',views.details,name='suggestions_details'),
    path('create/',views.create,name='suggestions_create'),
    path('update/<int:id>',views.update,name='suggestions_update'),
    path('delete/<int:id>',views.delete,name='suggestions_delete'),
]

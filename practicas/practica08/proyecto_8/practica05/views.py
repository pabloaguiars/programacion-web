from django.shortcuts import render
from .models import CURP
from .forms import CURPForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = CURP.objects.all()
    context = {
        'curps':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = CURP.objects.get(id=id)
    context = {
        'curp':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = CURPForm(request.POST or None)
    if request.user.is_authenticated:
        message = 'User is logged'
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'CURP saved'
            else:
                message = 'Invalid form'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    curp = CURP.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = curp
        message = 'User is logged'
        if request.method == 'POST':
            form = CURPForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'CURP updated'
            else:
                message = 'Invalid form'
        else:
            form = CURPForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    curp = CURP.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = curp
        message = 'CURP is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'CURP deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

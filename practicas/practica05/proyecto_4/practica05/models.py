from django.db import models
from django import forms
# from django.forms.widgets import ChoiceField

# Create your models here.
class JobApplication(models.Model):
    surnames = models.CharField(max_length=35)
    name = models.CharField(max_length=20)
    email = models.EmailField()
    web_site = models.CharField(max_length=255)
    position = models.CharField(max_length=60)
    salary = models.DecimalField(decimal_places=2,max_digits=7,default=0)
    initiation_date = models.DateField()
    phone = models.CharField(max_length=10)
    telephone = models.CharField(max_length=10)
    relocalization = models.CharField(
        max_length=3,
        choices=(
            ('yes','Sí'),
            ('no','No')
        ),
        default='no'
    )
    company_last_job = models.CharField(max_length=50)
    references = models.TextField(max_length=144)

#!/bin/bash

for i in {2..10}
do
    # clear terminal
    clear
    # change to folder project
    cd proyecto_$(echo $i)
    #python manage.py startapp practica05
    # wait few seconds
    #sleep 2
    # create files
    #touch practica05/urls.py
    #touch practica05/forms.py
    #return to main folder
    # make folder templates for app
    mkdir templates/practica05    
    cd ..
    # next
done

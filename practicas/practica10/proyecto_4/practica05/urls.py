from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='jobapplications_index'),
    path('list/',views.list,name='jobapplications_list'),
    path('details/<int:id>',views.details,name='jobapplications_details'),
    path('create/',views.create,name='jobapplications_create'),
    path('update/<int:id>',views.update,name='jobapplications_update'),
    path('delete/<int:id>',views.delete,name='jobapplications_delete'),
]

from django.shortcuts import render
from .models import JobApplication
from .forms import JobApplicationForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = JobApplication.objects.all()
    context = {
        'jobapplications':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = JobApplication.objects.get(id=id)
    context = {
        'jobapplication':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = JobApplicationForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Job application saved'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    jobapplication = JobApplication.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = jobapplication
        message = 'User is logged'
        if request.method == 'POST':
            form = JobApplicationForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Job application updated'
            else:
                message = 'Invalid form'
        else:
            form = JobApplicationForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    jobapplication = JobApplication.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = jobapplication
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Job application deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

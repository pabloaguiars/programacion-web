from django import forms
from .models import JobApplication

class JobApplicationForm(forms.ModelForm):
    class Meta:
        model = JobApplication
        fields = [
            'surnames',
            'name',
            'email',
            'web_site',
            'position',
            'salary',
            'initiation_date',
            'phone',
            'telephone',
            'relocalization',
            'company_last_job',
            'references'
        ]

from django import forms
from .models import User

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            'mother_surname',
            'father_surname',
            'name',
            'email',
            'password',
            'phone',
            'gender',
            'birthday_day',
            'birthday_month',
            'birthday_year',
        ]

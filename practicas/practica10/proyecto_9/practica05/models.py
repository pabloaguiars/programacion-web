from django.db import models
from django.utils.timezone import now

# Create your models here.
class User(models.Model):
    father_surname = models.CharField(max_length=50)
    mother_surname = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    email = models.EmailField()
    password = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    genders = (
        ('0','Female'),
        ('1','Male'),
    )
    gender = models.CharField(max_length=1,choices=genders)
    birthday_day = models.PositiveSmallIntegerField(default=1)
    birthday_month = models.PositiveSmallIntegerField(default=1)
    birthday_year = models.PositiveIntegerField(default=2019)
    slug = models.SlugField(null=True,editable=False,default=now)

    def __str__(self):
        return self.email

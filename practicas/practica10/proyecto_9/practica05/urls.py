from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='users_index'),
    path('list/',views.list,name='users_list'),
    path('details/<int:id>',views.details,name='users_details'),
    path('create/',views.create,name='users_create'),
    path('update/<int:id>',views.update,name='users_update'),
    path('delete/<int:id>',views.delete,name='users_delete'),
]

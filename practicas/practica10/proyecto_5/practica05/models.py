from django.db import models
from django.utils.timezone import now

# Create your models here.
class WasteReduction(models.Model):
    company = models.CharField(max_length=50)
    company_type = models.CharField(
        max_length=20,
        choices=(
            ('1','Association'),
            ('2','Sector chamber'),
            ('3','Certifier'),
            ('4','Marketer'),
            ('5','Cooperative'),
            ('6','Company'),
            ('7','Educational'),
            ('8','Exporter'),
            ('9','Federation'),
            ('10','Foundation'),
            ('11','Government agency'),
            ('12','International organization'),
            ('13','Non-governmental organization'),
        )
    )
    initiative_name = models.CharField(max_length=50)
    start_date = models.DateField()
    geographic_scope = models.CharField(
        max_length=20,
        choices=(
            ('1','National'),
            ('2','Provincial'),
            ('3','Local')
        )
    )
    state = models.CharField(max_length=50)
    locality = models.CharField(max_length=50)
    work_axes = models.CharField(
        max_length=20,
        choices=(
            ('1','Research'),
            ('2','Technology'),
            ('3','Legislation'),
            ('4','Communication'),
            ('5','Education'),
        )
    )
    sector = models.CharField(
        max_length=20,
        choices=(
            ('1','Primary production'),
            ('2','Industry and processing'),
            ('3','Transportation'),
            ('4','Marketing'),
            ('5','Food service'),
            ('5','Homes'),
            ('5','Other'),
        )
    )
    resume = models.TextField(max_length=255)
    in_action = models.CharField(
        max_length=2,
        choices=(
            ('1','Sí'),
            ('0','No'),
        )
    )
    number_of_beneficiaries = models.IntegerField()
    contact_name = models.CharField(max_length=150)
    contact_email = models.EmailField()
    action_plan_file = models.FileField(upload_to='files/%Y/%m/%d/',null=True)
    spread = models.CharField(
        max_length=2,
        choices=(
            ('1','Sí'),
            ('0','No'),
        )
    )
    slug = models.SlugField(null=True,editable=False,default=now)

    def __str__(self):
        return self.initiative_name

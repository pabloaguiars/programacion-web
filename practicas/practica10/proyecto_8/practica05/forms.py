from django import forms
from .models import CURP

class CURPForm(forms.ModelForm):
    class Meta:
        model = CURP
        fields = [
            'father_surname',
            'mother_surname',
            'name',
            'gender',
            'birthday_day',
            'birthday_month',
            'birthday_year',
            'state',
        ]

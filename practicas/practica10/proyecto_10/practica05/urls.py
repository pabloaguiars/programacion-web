from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='carrents_index'),
    path('list/',views.list,name='carrents_list'),
    path('details/<int:id>',views.details,name='carrents_details'),
    path('create/',views.create,name='carrents_create'),
    path('update/<int:id>',views.update,name='carrents_update'),
    path('delete/<int:id>',views.delete,name='carrents_delete'),
]

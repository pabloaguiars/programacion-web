from django import forms
from .models import CarRent

class CarRentForm(forms.ModelForm):
    class Meta:
        model = CarRent
        fields = [
            'father_surname',
            'mother_surname',
            'name',
            'phone',
            'address',
            'city',
            'state',
            'city',
            'state',
            'zip',
            'email',
            'car_type',
            'cars_quantity',
            'drivers_quantity',
            'freigth_type',
            'area_type',
            'international',
            'liability_limit',
            'charge_limit',
            'extra_information',
        ]

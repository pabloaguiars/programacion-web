from django.db import models
from django.utils.timezone import now

# Create your models here.
class BirthCertificate(models.Model):
    type = models.CharField(
        max_length=1,
        choices=(
            ('1','Literal'),
            ('2','Multilengual'),
        )
    )
    justification = models.TextField(max_length=144)
    name = models.CharField(max_length=50)
    mother_surname = models.CharField(max_length=50)
    father_surname = models.CharField(max_length=50)
    gender = models.CharField(
        max_length=1,
        choices=(
            ('0','Female'),
            ('1','Male'),
        )
    )
    birthday_day = models.IntegerField(default=1)
    birthday_month = models.IntegerField(default=1)
    birthday_year = models.IntegerField(default=2019)
    state = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    copies = models.CharField(
        max_length=1,
        choices=(
            ('0','1'),
            ('1','2'),
        )
    )
    telephone = models.CharField(max_length=10)
    phone = models.CharField(max_length=10)
    address = models.CharField(max_length=150)
    slug = models.SlugField(editable=False,null=True,default=now)

    def __str__(self):
        return self.email

from django.shortcuts import render
from .models import BirthCertificate
from .forms import BirthCertificateForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = BirthCertificate.objects.all()
    context = {
        'birthcertificates':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = BirthCertificate.objects.get(id=id)
    context = {
        'birthcertificate':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = BirthCertificateForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Birth certificate saved'
            else:
                message = 'Invalid form'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    birthcertificate = BirthCertificate.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = birthcertificate
        message = 'User is logged'
        if request.method == 'POST':
            form = BirthCertificateForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Checkout updated'
            else:
                message = 'Invalid form'
        else:
            form = BirthCertificateForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    birthcertificate = BirthCertificate.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = birthcertificate
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Birth certificate deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

from django.db import models
from django.utils.timezone import now

# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=50)
    mother_surname = models.CharField(max_length=50)
    father_surname = models.CharField(max_length=50)
    CURP = models.CharField(max_length=18)
    NSS = models.CharField(max_length=20)
    gender = models.CharField(
        max_length=1,
        choices=(
            ('F','Female'),
            ('M','Male'),
        )
    )
    Birthday = models.DateField()
    Birth_place = models.CharField(max_length=150)
    email = models.EmailField()
    telephone = models.CharField(max_length=10)
    phone = models.CharField(max_length=10)
    address = models.CharField(max_length=150)
    slug = models.SlugField(null=True,default=now,editable=False)

    def __str__(self):
        return self.email

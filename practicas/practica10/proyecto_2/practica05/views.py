from django.shortcuts import render
from .models import IREQ
from .forms import IREQForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = IREQ.objects.all()
    context = {
        'ireqs':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = IREQ.objects.get(id=id)
    context = {
        'ireq':object,
    }
    return render(request,'practica05/details.html',context)
from django.shortcuts import render

def create(request):
    form = IREQForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'IREQ saved'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    ireq = IREQ.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = ireq
        message = 'User is logged'
        if request.method == 'POST':
            form = IREQForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'IREQ updated'
            else:
                message = 'Invalid form'
        else:
            form = IREQForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    ireq = IREQ.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = ireq
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'IREQ deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

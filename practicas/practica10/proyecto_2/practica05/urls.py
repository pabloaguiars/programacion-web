from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='ireqs_index'),
    path('list/',views.list,name='ireqs_list'),
    path('details/<int:id>',views.details,name='ireqs_details'),
    path('create/',views.create,name='ireqs_create'),
    path('update/<int:id>',views.update,name='ireqs_update'),
    path('delete/<int:id>',views.delete,name='ireqs_delete'),
]

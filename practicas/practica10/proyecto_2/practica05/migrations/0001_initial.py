# Generated by Django 2.2.5 on 2019-09-16 20:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='IREQ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('receptor', models.CharField(choices=[('RECEPTOR1', 'RECEPTOR1'), ('RECEPTOR2', 'RECEPTOR2')], max_length=20)),
                ('ship_type', models.CharField(choices=[('GROUND', 'GROUND'), ('AIR', 'AIR')], max_length=20)),
                ('supplier', models.CharField(choices=[('SUPPLIER1', 'SUPPLIER1'), ('SUPPLIER2', 'SUPPLIER2')], max_length=20)),
                ('business_unit', models.CharField(choices=[('BUSINESSUNIT1', 'BUSINESSUNIT1'), ('BUSINESSUNIT2', 'BUSINESSUNIT2')], max_length=20)),
                ('account', models.CharField(choices=[('ACCOUNT1', 'ACCOUNT1'), ('ACCOUNT2', 'ACCOUNT2')], max_length=20)),
                ('currency', models.CharField(choices=[('CURRENCY1', 'CURRENCY1'), ('CURRENCY2', 'CURRENCY2')], max_length=20)),
                ('date', models.DateField()),
                ('description', models.TextField(max_length=144)),
                ('slug', models.SlugField()),
            ],
        ),
    ]

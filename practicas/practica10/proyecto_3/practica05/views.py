from django.shortcuts import render
from .models import QuejaSugerencias
from .forms import QuejaSugerenciasForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = QuejaSugerencias.objects.all()
    context = {
        'suggestions':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = QuejaSugerencias.objects.get(id=id)
    context = {
        'suggestion':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = QuejaSugerenciasForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Suggestion saved. Thank you!'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    quejaSugerencias = QuejaSugerencias.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = quejaSugerencias
        message = 'User is logged'
        if request.method == 'POST':
            form = QuejaSugerenciasForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Suggestion updated'
            else:
                message = 'Invalid form'
        else:
            form = QuejaSugerenciasForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    suggestion = QuejaSugerencias.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = suggestion
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Suggestion deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

from django.db import models
from django import forms
from django.forms.widgets import CheckboxSelectMultiple

# Create your models here.
class QuejaSugerencias(models.Model):
    subject = models.CharField(
        max_length=50,
        choices=(
            ('INFORMATION','Solicitud de Información'),
            ('SUGGESTION','Sugerencia'),
            ('COMPLAINT','Queja'),
            ('OTHER','Otro'),
        )
    )
    name = models.CharField(max_length=50)
    email = models.EmailField()
    company = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    message = models.TextField(max_length=144)
    cc = forms.MultipleChoiceField(
        choices=(
            (True,'Sí'),
            (False,'False'),
        ),
        widget = forms.CheckboxSelectMultiple()
    )

    def __str__(self):
        return 'Name: %s, Email: %s, Subject: %s' % (self.name, self.email, self.subject)

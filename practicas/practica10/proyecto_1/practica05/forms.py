from django import forms
from .models import Checkout

class CheckoutForm(forms.ModelForm):
    class Meta:
        model = Checkout
        fields = [
            'username',
            'email',
            'address',
            'city',
            'state',
            'zip',
            'card_name',
            'card_number',
            'card_exp_month',
            'card_exp_year',
            'card_security_code',
            'slug',
        ]

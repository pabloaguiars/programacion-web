#!/bin/bash

for i in {1..10}
do
    # clear terminal
    clear
    # create folder
    mkdir $(echo $i)
    cd $(echo $i)
    # create virtualenv
    virtualenv -p python3 venv-$(echo $i)
    # activate virtualenv
    source venv-$(echo $i)/bin/activate
    # install django
    pip install django
    # create project
    django-admin startproject proyecto_$(echo $i)
    # run server
    python proyecto_$(echo $i)/manage.py runserver
    # wait few seconds
    sleep 2
    # desactivate virtualenv
    deactivate
    #return to main folder
    cd ..
    # next
done

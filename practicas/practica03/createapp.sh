#!/bin/bash

for i in {2..10}
do
    # clear terminal
    clear
    # change folder
    cd $(echo $i)
    # activate virtualenv
    source venv-$(echo $i)/bin/activate
    # change to project django folder
    cd proyecto_$(echo $i)
    # create app
    python manage.py startapp newapp_$(echo $i)
    # create urls file
    touch newapp_$(echo $i)/urls.py
    # return folder
    cd ..
    # desactivate virtualenv
    deactivate
    # return to main folder
    cd ..
    # next
done

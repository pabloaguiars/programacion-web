from django import forms
from .models import Student

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = [
            'name',
            'mother_surname',
            'father_surname',
            'CURP',
            'NSS',
            'gender',
            'Birthday',
            'Birth_place',
            'email',
            'telephone',
            'phone',
            'address',
        ]

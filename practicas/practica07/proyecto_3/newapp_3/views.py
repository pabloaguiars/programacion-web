from django.shortcuts import render

# Create your views here.
def checkout(request):
    context={

    }

    return render(request,'newapp_3/checkout.html',context)

def ireq(request):
    context={

    }

    return render(request,'newapp_3/formato_ireq.html',context)

def quejas(request):
    context={

    }

    return render(request,'newapp_3/formulario_quejas_sugerencias.html',context)

def empleo(request):
    context={

    }

    return render(request,'newapp_3/solicitud_empleo.html',context)

def desperdicios(request):
    context={

    }

    return render(request,'newapp_3/solicitud_reduccion_desperdicios.html',context)

def alumno(request):
    context={

    }

    return render(request,'newapp_3/solicitud_registro_alumno.html',context)

def civil(request):
    context={

    }

    return render(request,'newapp_3/solicitud_registro_civil.html',context)

def curp(request):
    context={

    }

    return render(request,'newapp_3/solicitud_registro_curp.html',context)

def usuario(request):
    context={

    }

    return render(request,'newapp_3/solicitud_registro_usuario.html',context)

def renta(request):
    context={

    }

    return render(request,'newapp_3/solicitud_renta_automovil.html',context)

def index(request):
    context={

    }

    return render(request,'newapp_3/index.html',context)

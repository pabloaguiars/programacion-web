from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='suggestions_index'),
    path('list/',views.list,name='suggestions_list'),
    path('details/<int:id>',views.details,name='suggestions_details'),
    path('create/',views.create,name='suggestions_create'),
]

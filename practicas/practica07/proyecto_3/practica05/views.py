from django.shortcuts import render
from .models import QuejaSugerencias
from .forms import QuejaSugerenciasForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = QuejaSugerencias.objects.all()
    context = {
        'suggestions':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = QuejaSugerencias.objects.get(id=id)
    context = {
        'suggestion':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = QuejaSugerenciasForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Suggestion saved. Thank you!'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

from django.shortcuts import render
from .models import IREQ
from .forms import IREQForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = IREQ.objects.all()
    context = {
        'ireqs':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = IREQ.objects.get(id=id)
    context = {
        'ireq':object,
    }
    return render(request,'practica05/details.html',context)
from django.shortcuts import render

def create(request):
    form = IREQForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'IREQ saved'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

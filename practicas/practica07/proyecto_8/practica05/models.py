from django.db import models
from django.utils.timezone import now

# Create your models here.
class CURP(models.Model):
    father_surname = models.CharField(max_length=50)
    mother_surname = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    genders = (
        ('0','Female'),
        ('1','Male'),
    )
    gender = models.CharField(max_length=1,choices=genders)
    birthday_day = models.IntegerField(default=1)
    birthday_month = models.IntegerField(default=1)
    birthday_year = models.IntegerField(default=2019)
    state = models.CharField(max_length=50)
    slug = models.SlugField(null=True,editable=False,default=now)

    def __str__(self):
        return '%s %s %s' % (father_surname, mother_surname, name)

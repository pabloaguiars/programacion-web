from django.shortcuts import render
from .models import WasteReduction
from .forms import WasteReductionForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = WasteReduction.objects.all()
    context = {
        'wastereductions':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = WasteReduction.objects.get(id=id)
    context = {
        'wastereduction':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = WasteReductionForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Waste reductions suggestion saved'
            else:
                message = 'Some field in form did not fill correctly'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

from django import forms
from .models import WasteReduction

class WasteReductionForm(forms.ModelForm):
    class Meta:
        model = WasteReduction
        fields = [
            'company',
            'company_type',
            'initiative_name',
            'start_date',
            'geographic_scope',
            'state',
            'locality',
            'work_axes',
            'sector',
            'resume',
            'in_action',
            'number_of_beneficiaries',
            'contact_name',
            'contact_email',
            'spread',
        ]

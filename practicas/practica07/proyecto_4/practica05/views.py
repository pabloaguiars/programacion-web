from django.shortcuts import render
from .models import JobApplication
from .forms import JobApplicationForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = JobApplication.objects.all()
    context = {
        'jobapplications':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = JobApplication.objects.get(id=id)
    context = {
        'jobapplication':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = JobApplicationForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Job application saved'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

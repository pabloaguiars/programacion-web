from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='carrents_index'),
    path('list/',views.list,name='carrents_list'),
    path('details/<int:id>',views.details,name='carrents_details'),
    path('create/',views.create,name='carrents_create'),
]

from newapp_7 import views
from django.urls import path

urlpatterns = [
    path('checkout',views.checkout,name='checkout'),
    path('ireq',views.ireq,name='ireq'),
    path('quejas',views.quejas,name='quejas'),
    path('empleo',views.empleo,name='empleo'),
    path('desperdicios',views.desperdicios,name='desperdicios'),
    path('alumno',views.alumno,name='alumno'),
    path('civil',views.civil,name='civil'),
    path('curp',views.curp,name='curp'),
    path('usuario',views.usuario,name='usuario'),
    path('renta',views.renta,name='renta'),
    path('index',views.index,name='index'),
]

from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='birthcertificates_index'),
    path('list/',views.list,name='birthcertificates_list'),
    path('details/<int:id>',views.details,name='birthcertificates_details'),
    path('create/',views.create,name='birthcertificate_create'),
]

from django import forms
from .models import BirthCertificate

class BirthCertificateForm(forms.ModelForm):
    class Meta:
        model = BirthCertificate
        fields = [
            'type',
            'justification',
            'name',
            'mother_surname',
            'father_surname',
            'gender',
            'birthday_day',
            'birthday_month',
            'birthday_year',
            'state',
            'email',
            'copies',
            'telephone',
            'phone',
            'address',
        ]

from django.contrib import admin
from practica05.models import BirthCertificate

# Register your models here.
admin.site.register(BirthCertificate)

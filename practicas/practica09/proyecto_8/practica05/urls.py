from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='curps_index'),
    path('list/',views.list,name='curps_list'),
    path('details/<int:id>',views.details,name='curps_details'),
    path('create/',views.create,name='curps_create'),
    path('update/<int:id>',views.update,name='curps_update'),
    path('delete/<int:id>',views.delete,name='curps_delete'),
]

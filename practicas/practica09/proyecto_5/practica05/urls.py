from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='wastereductions_index'),
    path('list/',views.list,name='wastereductions_list'),
    path('details/<int:id>',views.details,name='wastereductions_details'),
    path('create/',views.create,name='wastereductions_create'),
    path('update/<int:id>',views.update,name='wastereductions_update'),
    path('delete/<int:id>',views.delete,name='wastereductions_delete'),
]

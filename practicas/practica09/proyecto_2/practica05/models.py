from django.db import models

# Create your models here.
class IREQ(models.Model):
    receptor = models.CharField(
        max_length=20,
        choices=(
            ('RECEPTOR1','RECEPTOR1'),
            ('RECEPTOR2','RECEPTOR2'),
        )
    )
    ship_type = models.CharField(
        max_length=20,
        choices=(
            ('GROUND','GROUND'),
            ('AIR','AIR'),
        )
    )
    supplier = models.CharField(
        max_length=20,
        choices=(
            ('SUPPLIER1','SUPPLIER1'),
            ('SUPPLIER2','SUPPLIER2'),
        )
    )
    business_unit = models.CharField(
        max_length=20,
        choices=(
            ('BUSINESSUNIT1','BUSINESSUNIT1'),
            ('BUSINESSUNIT2','BUSINESSUNIT2'),
        )
    )
    account = models.CharField(
        max_length=20,
        choices=(
            ('ACCOUNT1','ACCOUNT1'),
            ('ACCOUNT2','ACCOUNT2'),
        )
    )
    currency = models.CharField(
        max_length=20,
        choices=(
            ('CURRENCY1','CURRENCY1'),
            ('CURRENCY2','CURRENCY2'),
        )
    )
    date = models.DateField()
    description = models.TextField(max_length=144)
    slug = models.SlugField()

    def __str__(self):
        return self.description

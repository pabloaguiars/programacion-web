from django import forms
from .models import IREQ

class IREQForm(forms.ModelForm):
    date = forms.DateField()
    class Meta:
        model = IREQ
        fields = [
            'receptor',
            'ship_type',
            'supplier',
            'business_unit',
            'account',
            'currency',
            'date',
            'description',
            'slug',
        ]

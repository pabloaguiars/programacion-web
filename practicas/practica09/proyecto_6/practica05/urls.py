from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='students_index'),
    path('list/',views.list,name='students_list'),
    path('details/<int:id>',views.details,name='students_details'),
    path('create/',views.create,name='students_create'),
    path('update/<int:id>',views.update,name='students_update'),
    path('delete/<int:id>',views.delete,name='students_delete'),
]

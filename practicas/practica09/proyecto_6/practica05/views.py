from django.shortcuts import render
from .models import Student
from .forms import StudentForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = Student.objects.all()
    context = {
        'students':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = Student.objects.get(id=id)
    context = {
        'student':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = StudentForm(request.POST or None)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Student saved'
            else:
                message = 'Invalid form'
        else:
            message = 'User is logged'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    student = Student.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = student
        message = 'User is logged'
        if request.method == 'POST':
            form = StudentForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Student updated'
            else:
                message = 'Invalid form'
        else:
            form = StudentForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    student = Student.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = student
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Student deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

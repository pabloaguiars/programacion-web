from django.db import models
from django.utils.timezone import now

# Create your models here.
class CarRent(models.Model):
    father_surname = models.CharField(max_length=50)
    mother_surname = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    address = models.CharField(max_length=150)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    zip = models.PositiveIntegerField()
    email = models.EmailField()
    cars_types=(
        ('1','Tractor'),
        ('2','Cargo truck'),
        ('3','Bus'),
        ('4','Limousine'),
        ('5','Other'),
    )
    freigth_types=(
        ('1','General'),
        ('2','Building material'),
        ('3','Chilled'),
        ('5','Other'),
    )
    area_types=(
        ('1','Local 0-50 Kms'),
        ('2','Intermediate 50 - 200 Kms'),
        ('3','Regional 200 - 500 Kms'),
        ('4','National 500+ kms'),
    )
    liability_limits=(
        ('1','$300,000'),
        ('2','$500,000'),
        ('3','$750,000'),
        ('4','$1,000,000'),
        ('5','+1,000,000'),
    )
    charge_limits=(
        ('1','$50,000'),
        ('2','$100,000'),
        ('3','$250,000'),
        ('4','+$250,000'),
    )
    car_type = models.CharField(max_length=15,choices=cars_types)
    cars_quantity = models.PositiveIntegerField()
    drivers_quantity = models.PositiveIntegerField()
    freigth_type = models.CharField(max_length=15,choices=freigth_types)
    area_type = models.CharField(max_length=15,choices=area_types)
    international = models.CharField(
        max_length=1,
        choices=(
            ('0','No'),
            ('1','Yes'),
        )
    )
    insurance = models.CharField(
        max_length=1,
        choices=(
            ('0','No'),
            ('1','Yes'),
        )
    )
    liability_limit = models.CharField(max_length=15,choices=liability_limits)
    charge_limit = models.CharField(max_length=15,choices=charge_limits)
    extra_information = models.TextField(max_length=144, default='none')
    slug = models.SlugField(null=True,editable=False,default=now)

    def __str__(self):
        return self.email

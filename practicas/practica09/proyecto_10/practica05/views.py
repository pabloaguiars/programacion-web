from django.shortcuts import render
from .models import CarRent
from .forms import CarRentForm

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = CarRent.objects.all()
    context = {
        'carrents':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = CarRent.objects.get(id=id)
    context = {
        'carrent':object,
    }
    return render(request,'practica05/details.html',context)

def create(request):
    form = CarRentForm(request.POST or None)
    if request.user.is_authenticated:
        message = 'User is logged'
        if request.method == 'POST':
            if form.is_valid():
                instance = form.save(commit=False)
                instance.save()
                message = 'Car rent saved'
            else:
                message = 'Invalid form'
    else:
        message = 'User must be logged'

    context = {
        'form':form,
        'message':message,
    }

    return render(request,'practica05/create.html',context)

def update(request,id):
    carrent = CarRent.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = carrent
        message = 'User is logged'
        if request.method == 'POST':
            form = CarRentForm(request.POST, instance=instance)
            if form.is_valid():
                instance.save()
                message = 'Car rent updated'
            else:
                message = 'Invalid form'
        else:
            form = CarRentForm(instance=instance)

        context['form'] = form
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/update.html',context)

def delete(request,id):
    carrent = CarRent.objects.get(id=id)
    context = {}

    if request.user.is_authenticated:
        instance = carrent
        message = 'User is logged'
        if request.method == 'POST':
            instance.delete()
            message = 'Car rent deleted'
            context['instance'] = ''
        else:
            context['instance'] = instance
    else:
        message = 'User must be logged'

    context['message'] = message

    return render(request,'practica05/delete.html',context)

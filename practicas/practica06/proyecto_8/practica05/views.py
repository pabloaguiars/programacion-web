from django.shortcuts import render
from .models import CURP

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = CURP.objects.all()
    context = {
        'curps':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = CURP.objects.get(id=id)
    context = {
        'curp':object,
    }
    return render(request,'practica05/details.html',context)

from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='curps_index'),
    path('list/',views.list,name='curps_list'),
    path('details/<int:id>',views.details,name='curps_details'),
]

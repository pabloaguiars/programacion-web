from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='wastereductions_index'),
    path('list/',views.list,name='wastereductions_list'),
    path('details/<int:id>',views.details,name='wastereductions_details'),
]

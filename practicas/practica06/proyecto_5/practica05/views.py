from django.shortcuts import render
from .models import WasteReduction

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = WasteReduction.objects.all()
    context = {
        'wastereductions':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = WasteReduction.objects.get(id=id)
    context = {
        'wastereduction':object,
    }
    return render(request,'practica05/details.html',context)

from django.shortcuts import render
from .models import User

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = User.objects.all()
    context = {
        'users':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = User.objects.get(id=id)
    context = {
        'user':object,
    }
    return render(request,'practica05/details.html',context)

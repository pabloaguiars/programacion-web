from django.shortcuts import render
from .models import CarRent

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = CarRent.objects.all()
    context = {
        'carrents':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = CarRent.objects.get(id=id)
    context = {
        'carrent':object,
    }
    return render(request,'practica05/details.html',context)

from django.shortcuts import render
from .models import Student

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = Student.objects.all()
    context = {
        'students':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = Student.objects.get(id=id)
    context = {
        'student':object,
    }
    return render(request,'practica05/details.html',context)

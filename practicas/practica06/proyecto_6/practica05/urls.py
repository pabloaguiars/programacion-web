from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='students_index'),
    path('list/',views.list,name='students_list'),
    path('details/<int:id>',views.details,name='students_details'),
]

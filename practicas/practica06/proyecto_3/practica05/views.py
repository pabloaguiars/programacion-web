from django.shortcuts import render
from .models import QuejaSugerencias

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = QuejaSugerencias.objects.all()
    context = {
        'suggestions':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = QuejaSugerencias.objects.get(id=id)
    context = {
        'suggestion':object,
    }
    return render(request,'practica05/details.html',context)

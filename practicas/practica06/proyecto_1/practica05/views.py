from django.shortcuts import render
from .models import Checkout

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = Checkout.objects.all()
    context = {
        'checkouts':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = Checkout.objects.get(id=id)
    context = {
        'checkout':object,
    }
    return render(request,'practica05/details.html',context)

from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='checkouts_index'),
    path('list/',views.list,name='checkouts_list'),
    path('details/<int:id>',views.details,name='checkouts_details'),
]

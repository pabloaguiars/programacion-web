from django.db import models

# Create your models here.
class Checkout(models.Model):
    username = models.CharField(max_length=50)
    email = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=150)
    state = models.CharField(max_length=150)
    zip = models.PositiveIntegerField()
    card_name  = models.CharField(max_length=50)
    card_number  = models.CharField(max_length=16)
    card_exp_month = models.PositiveSmallIntegerField()
    card_exp_year = models.PositiveIntegerField()
    card_security_code = models.PositiveSmallIntegerField()
    slug = models.SlugField()

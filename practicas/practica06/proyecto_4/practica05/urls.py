from django.urls import path
from practica05 import views

urlpatterns = [
    path('',views.index,name='jobapplications_index'),
    path('list/',views.list,name='jobapplications_list'),
    path('details/<int:id>',views.details,name='jobapplications_details'),
]

from django.shortcuts import render
from .models import JobApplication

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = JobApplication.objects.all()
    context = {
        'jobapplications':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = JobApplication.objects.get(id=id)
    context = {
        'jobapplication':object,
    }
    return render(request,'practica05/details.html',context)

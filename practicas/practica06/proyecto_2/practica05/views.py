from django.shortcuts import render
from .models import IREQ

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = IREQ.objects.all()
    context = {
        'ireqs':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = IREQ.objects.get(id=id)
    context = {
        'ireq':object,
    }
    return render(request,'practica05/details.html',context)
from django.shortcuts import render

# Create your views here.

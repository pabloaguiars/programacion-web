from django.shortcuts import render
from .models import BirthCertificate

# Create your views here.
def index(request):
    context = {

    }
    return render(request,'practica05/index.html',context)

def list(request):
    all_objects = BirthCertificate.objects.all()
    context = {
        'birthcertificates':all_objects,
    }
    return render(request,'practica05/list.html',context)

def details(request,id):
    object = BirthCertificate.objects.get(id=id)
    context = {
        'birthcertificate':object,
    }
    return render(request,'practica05/details.html',context)
